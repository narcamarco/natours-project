require('dotenv').config();

process.on('uncaughtException', (err) => {
  console.log(`UNCAUGHT Exception! shutting down`);

  process.exit(1);
});

const app = require('./app');
const connectDB = require('./db/connect');

const PORT = process.env.PORT || 3000;
const start = async () => {
  try {
    await connectDB(process.env.DATABASE);
    app.listen(PORT, () => {
      console.log(`Server is listening on port ${PORT}...`);
    });
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};

start();

// Unhandled Promise Rejection
process.on('unhandledRejection', (err) => {
  console.log(`UNHANDLED REJECTION! shutting down`);

  process.exit(1);
});

// mongoose
//   .connect(process.env.DATABASE, {
//     useNewUrlParser: true,
//     useCreateIndex: true,
//     useFindAndModify: false,
//     useUnifiedTopology: true,
//   })
//   .then(() => {
//     console.log(`DB connection successful!`);
//   });

// const PORT = process.env.PORT || 3000;
// app.listen(PORT, () => {
//   console.log(`App running on port ${PORT}....`);
// });

// app.get('/', (req, res) => {
//   res.send('Hello World');
// });
