
export const displayMap = (locations) => {
  mapboxgl.accessToken =
    'pk.eyJ1IjoibmFyY2FtYXJjbyIsImEiOiJjbGRhNXJiNTIwMDRoM3hwdXViMnk4bGU4In0.cYTuYzK-tp6wvvIcLuIJVQ';

  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/narcamarco/clda6pjcx001x01o0y2ze78ax',
    scrollZoom: false,
  });

  const bounds = new mapboxgl.LngLatBounds();

  locations.forEach((loc) => {
    // create marker
    const el = document.createElement('div');
    el.className = 'marker';
    // add marker
    new mapboxgl.Marker({
      element: el,
      anchor: 'bottom',
    })
      .setLngLat(loc.coordinates)
      .addTo(map);

    // Add popup
    new mapboxgl.Popup({
      offset: 30,
    })
      .setLngLat(loc.coordinates)
      .setHTML(`<p>Day ${loc.day}: ${loc.description}</p>`)
      .addTo(map);
    // extends map bounds to include current location
    bounds.extend(loc.coordinates);
  });

  map.fitBounds(bounds, {
    padding: {
      top: 200,
      bottom: 150,
      left: 100,
      right: 100,
    },
  });
};
