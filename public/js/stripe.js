import axios from 'axios';

import { showAlert } from './alert';

export const bookTour = async (tourId) => {
  try {
    const stripe = Stripe(
      'pk_test_51M0mGtH3zFM5xXDsoHUzpmhkgo2a9ewhjiS9yS7j07rOHIblJdo0Sw5ZWZsUbk3GyyGdoNeLqutIuEg5q4R83oCI00GOmTZjRg'
    );
    const session = await axios(`/api/v1/bookings/checkout-session/${tourId}`);

    console.log(session.data.session.id);
    // console.log(session.data.response);
    await stripe.redirectToCheckout({
      sessionId: session.data.session.id,
    });
  } catch (error) {
    console.log(error);
    showAlert('error', err);
  }
};
