require('dotenv').config();
const fs = require('fs');
const connectDB = require('../../db/connect');
const Tour = require('../../models/tourModel');
const Review = require('../../models/reviewModel');
const User = require('../../models/userModel');

// mongoose
//   .connect(process.env.DATABASE, {
//     useNewUrlParser: true,
//     useCreateIndex: true,
//     useFindAndModify: false,
//     useUnifiedTopology: true,
//   })
//   .then(() => {
//     console.log(`DB connection successful!`);
//   });

// app.listen(PORT, () => {
//   console.log(`App running on port ${PORT}....`);
// });

const tours = JSON.parse(fs.readFileSync(`${__dirname}/tours.json`, 'utf-8'));
const users = JSON.parse(fs.readFileSync(`${__dirname}/users.json`, 'utf-8'));
const reviews = JSON.parse(
  fs.readFileSync(`${__dirname}/reviews.json`, 'utf-8')
);

const InsertData = async () => {
  try {
    await connectDB(process.env.DATABASE);
    await Tour.create(tours);
    await User.create(users, { validateBeforeSave: false });
    await Review.create(reviews);
    // console.log(`Data Successfully Loaded`);
    process.exit();
  } catch (error) {
    console.log(error);
  }
};

const deleteData = async () => {
  try {
    await connectDB(process.env.DATABASE);
    await Tour.deleteMany();
    await User.deleteMany();
    await Review.deleteMany();
    // console.log(`Data Successfully Delete`);
    process.exit();
  } catch (error) {
    console.log(error);
  }
};

if (process.argv[2] === '--import') {
  InsertData();
} else if (process.argv[2] === '--delete') {
  deleteData();
}
