const sgMail = require('@sendgrid/mail');
const pug = require('pug');
const htmlToText = require('html-to-text');

module.exports = class Email {
  constructor(user, url) {
    this.to = user.email;
    this.firstName = user.name.split(' ')[0];
    this.url = url;
    this.from = `Marco Narca <${process.env.EMAIL_USERNAME}>`;
  }

  createTransport() {
    // if (process.env.NODE_ENV === 'production') {
    //   // Send grid
    //   return 1;
    // }

    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  }

  async send(template, subject) {
    // Render Html based on Pug TEMPLATE
    const html = pug.renderFile(`${__dirname}/../views/email/${template}.pug`, {
      firstName: this.firstName,
      url: this.url,
      subject,
    });

    // Define email options
    const mailOptions = {
      to: this.to,
      from: this.from,
      subject,
      text: 'PASSWORD',
      html,
    };

    // send the email
    await sgMail.send(mailOptions);
  }

  async sendWelcome() {
    this.createTransport();
    await this.send('welcome', 'Welcome to the Natours Family');
  }

  async sendPasswordReset() {
    this.createTransport();
    await this.send(
      'passwordReset',
      'Your password reset token (valid only for 10 minutes)'
    );
  }
};

// const sendEmail = async (options) => {
//   sgMail.setApiKey(process.env.SENDGRID_API_KEY);

//   // define the email options

//   const mailOptions = {
//     to: options.email,
//     from: 'Marco Narca <marco@pbusinesssolutions.com>',
//     subject: options.subject,
//     text: options.message,
//     html: `<p>${options.message}</p>.`,
//   };
//   console.log(`Sending Email`);
//   await sgMail.send(mailOptions);
// };

// module.exports = sendEmail;
